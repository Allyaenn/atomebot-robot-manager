#!/usr/bin/env python3
'''Robot control script
'''
import cozmo
import time
import json
import random
import meetingManager
import serverManager
       

### Defining Cozmo Program ####
def cozmo_program(robot: cozmo.robot.Robot):
    meeting_man = meetingManager.meetingManager()
    util_serv = serverManager.serverManager("http://localhost:8888")

    do_run = True

    try : 
        while do_run:
            time.sleep(5)
            msg = util_serv.requestMessageWithGET();
            if msg is None : 
                print("no message to express");
            else :
                # print("you've got mail ! : " + json.dumps(msg, separators=(',', ':')))
                # #data_msg = json.loads(msg)
                # print(msg["type"])
                enact_message(msg, robot,meeting_man, util_serv)
    except KeyboardInterrupt:
        print("ctrl-c catched - program is interrupted")

def enact_message(message, robot, meeting_man, util_serv):

    if len(message["targets"]) == 0:
        robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
        util_serv.informServerWithPOST(message);

    elif len(message["targets"]) == 1:
        robot.go_to_pose(meeting_man.dict_poses[message["targets"][0]]).wait_for_completed()
        robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
        robot.go_to_pose(meeting_man.dict_poses["origin"]).wait_for_completed()
        util_serv.informServerWithPOST(message);
        
    elif len(message["targets"]) == 2:  

        if message["targets"][0] in ["sujet", "temps", "discussion"] : 
            robot.go_to_pose(meeting_man.dict_poses[message["targets"][0]]).wait_for_completed()
            robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
            robot.go_to_pose(meeting_man.dict_poses[message["targets"][1]]).wait_for_completed()
            robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
            robot.go_to_pose(meeting_man.dict_poses["origin"]).wait_for_completed()
            util_serv.informServerWithPOST(message);

        elif message["targets"][1] in ["sujet", "temps", "discussion"] : 
            robot.go_to_pose(meeting_man.dict_poses[message["targets"][1]]).wait_for_completed()
            robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
            robot.go_to_pose(meeting_man.dict_poses[message["targets"][0]]).wait_for_completed()
            robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
            robot.go_to_pose(meeting_man.dict_poses["origin"]).wait_for_completed()
            util_serv.informServerWithPOST(message);

        elif message["targets"][0] not in ["sujet", "temps", "discussion"] and message["targets"][1] not in ["sujet", "temps", "discussion"] :
            rand_ind = random.randint(0,1)
            other_rand_in = (rand_ind+1)%2
            robot.go_to_pose(meeting_man.dict_poses[message["targets"][rand_ind]]).wait_for_completed()
            robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
            robot.go_to_pose(meeting_man.dict_poses[message["targets"][other_rand_in]]).wait_for_completed()
            robot.play_anim_trigger(robot.anim_triggers[meeting_man.dict_anims[message["emotion"]]]).wait_for_completed()
            robot.go_to_pose(meeting_man.dict_poses["origin"]).wait_for_completed()
            util_serv.informServerWithPOST(message);

        else : 
             print("ERROR :: Object message has invalid structure")

    else :
        print("ERROR :: Object message is invalid")
    
cozmo.run_program(cozmo_program)

