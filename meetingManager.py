import cozmo

#### Class Meeting ####
class meetingManager: 

    def __init__(self):
        self.dict_poses = {}
        self.dict_anims = {}
        self.createAllPoses()
        self.createAllAnims()
    
    def createAllPoses(self):
        self.dict_poses["origin"] = cozmo.util.Pose(0, 0, 0, angle_z=cozmo.util.degrees(0))
        self.dict_poses["sujet"] = cozmo.util.Pose(20, 70, 0, angle_z=cozmo.util.degrees(-45))
        self.dict_poses["temps"] = cozmo.util.Pose(20, -70, 0, angle_z=cozmo.util.degrees(45))
        self.dict_poses["discussion"] = cozmo.util.Pose(-90, 0, 0, angle_z=cozmo.util.degrees(-90))
        self.dict_poses["groupe"] = cozmo.util.Pose(-170, 0, 0, angle_z=cozmo.util.degrees(-90))
        self.dict_poses["A"] = cozmo.util.Pose(220, 180, 0, angle_z=cozmo.util.degrees(0))
        self.dict_poses["B"] = cozmo.util.Pose(220, -180, 0, angle_z=cozmo.util.degrees(0))
        self.dict_poses["C"] = cozmo.util.Pose(-220, -180, 0, angle_z=cozmo.util.degrees(180))
        self.dict_poses["D"] = cozmo.util.Pose(-220, 180, 0, angle_z=cozmo.util.degrees(180))
        # self.topic = cozmo.world.World.create_custom_fixed_object(topic_pose, 1, 1, 1, False, True)
        # self.time = cozmo.world.World.create_custom_fixed_object(time_pose, 1, 1, 1, False, True)
        # self.discussion = cozmo.world.World.create_custom_fixed_object(discussion_pose, 1, 1, 1, False, True)
        # self.group = cozmo.world.World.create_custom_fixed_object(group_pose, 1, 1, 1, False, True)
        print("Poses created")

    def createAllAnims(self):
        self.dict_anims["joie"] = 57
        self.dict_anims["colere"] = 54
        self.dict_anims["tristesse"] = 42
        self.dict_anims["peur"] = 69
        print("Anims created")