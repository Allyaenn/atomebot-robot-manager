# res
# robot.drive_straight(cozmo.util.distance_mm(100),cozmo.util.speed_mmps(200),False).wait_for_completed()
# robot.turn_in_place(cozmo.util.degrees(-90), False, 0, cozmo.util.degrees(90)).wait_for_completed()
# robot.say_text("Hé bulle, miaou").wait_for_completed()
# CodeLabScaredCozmo= _AnimTrigger(name='CodeLabScaredCozmo', id=69)
# PeekABooGetOutSad= _AnimTrigger(name='PeekABooGetOutSad', id=377)
# CodeLabFrustrated= _AnimTrigger(name='CodeLabFrustrated', id=54)
# CodeLabHappy= _AnimTrigger(name='CodeLabHappy', id=57)
# robot.drive_straight(cozmo.util.distance_mm(100),cozmo.util.speed_mmps(200),False).wait_for_completed()
# robot.turn_in_place(cozmo.util.degrees(45), False, 0, cozmo.util.degrees(90)).wait_for_completed()
# robot.turn_in_place(cozmo.util.degrees(-45), False, 0, cozmo.util.degrees(90)).wait_for_completed()
# robot.drive_straight(cozmo.util.distance_mm(200),cozmo.util.speed_mmps(200),False).wait_for_completed()
# robot.play_anim_trigger(robot.anim_triggers[54]).wait_for_completed()

 #setup ###############################################################################################
    anim_id = 57 #happy

    dist_zc = cozmo.util.distance_mm(110) # mm
    dist_zc_p = cozmo.util.distance_mm(120) # mm
    dist_zc_zc = cozmo.util.distance_mm(150) # mm
    dist_p = cozmo.util.distance_mm(200) # mm
    dist_p_p = cozmo.util.distance_mm(360) # mm
    dist_p_short_for = cozmo.util.distance_mm(20) # mm
    dist_p_short_back = cozmo.util.distance_mm(-20) # mm

    speed_st = cozmo.util.speed_mmps(300) # mm/s
    speed_st_short = cozmo.util.speed_mmps(300) # mm/s

    ang_zc_tps = cozmo.util.degrees(-45) # degrees
    ang_zc_p = cozmo.util.degrees(45) # degrees
    ang_zc_zc = cozmo.util.degrees(135) # degrees
    ang_p = cozmo.util.degrees(90) # degrees
    ang_p_p = cozmo.util.degrees(130) # degrees
    ang_speed = cozmo.util.degrees(180) # degrees
    # voir si d'autres sont nécéssaires

    # #behavior 1 (E) ###############################################################################################
    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    #behavior 2 (➡️ AP + E) ###############################################################################################
    # robot.drive_straight(dist_p,speed_st,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 3 (🔃 AP + E) ###############################################################################################
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.5)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 4 (➡️ Z Temps + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 5 (➡️ Z Temps + V + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.say_text("Discussion").wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 6 (🔃 Z Temps + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.5)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 7 (🔃 Z Temps + V + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    robot.say_text("Discussion", False, False, 0.7,1.0).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 8 (➡️ AP1 + ➡️ AP2 + E) ###############################################################################################
    # robot.drive_straight(dist_p,speed_st,False).wait_for_completed()
    # robot.turn_in_place(ang_p_p, False, 0, ang_speed).wait_for_completed()

    # time.sleep(1)

    # robot.drive_straight(dist_p_p,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 9 (➡️ AP1 + E + ➡️ AP2 + E) ###############################################################################################
    # robot.drive_straight(dist_p,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    # time.sleep(1)

    # robot.turn_in_place(ang_p_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_p,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 10 (🔃 AP1 + 🔃 AP2 + E ) ###############################################################################################
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(ang_p, False, 0, ang_speed).wait_for_completed()

    # time.sleep(0.2)

    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 11 (🔃 AP1 + E + 🔃 AP2 + E) ###############################################################################################
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(ang_p, False, 0, ang_speed).wait_for_completed()

    # time.sleep(0.2)

    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # # #behavior 12 (➡️ Z Temps + ➡️ AP + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(ang_zc_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc_p,speed_st,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(ang_zc_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(cozmo.util.degrees(-150), False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    #behavior 13 (➡️ Z Temps + 🔃 AP + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(ang_zc_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()


    # #behavior 14 (🔃 Z Temps + 🔃 AP + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.turn_in_place(ang_zc_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(0.2)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    # #behavior 15 (➡️ Z Groupe + ➡️ Z Temps  + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.turn_in_place(ang_zc_zc, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc_zc,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    # #behavior 16 (🔃 Z Groupe + 🔃 Z Temps  + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(1)

    # robot.turn_in_place(ang_zc_zc, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.drive_straight(dist_p_short_back,speed_st_short,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    #behavior 17 (➡️ Z Discussion + ➡️ Z Temps  + E) ###############################################################################################
    # robot.turn_in_place(ang_zc_tps, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.turn_in_place(ang_zc_zc, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc_zc,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.turn_in_place(ang_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_zc_zc,speed_st,False).wait_for_completed()

    # time.sleep(1)

    # robot.play_anim_trigger(robot.anim_triggers[anim_id]).wait_for_completed()

    # robot.turn_in_place(ang_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p,speed_st,False).wait_for_completed()

    import socketio

sio = socketio.Client()
sio.connect('http://localhost:8888')
print('my sid is', sio.sid)
sio.emit('python', 'Helloooo')

@sio.event
def connect():
    print("I'm connected!")

@sio.event
def connect_error():
    print("The connection failed!")

@sio.event
def disconnect():
    print("I'm disconnected!")

@sio.on('python_connected')
def on_message(data):
    print('I received a message from node !')

    # robot.go_to_pose(meeting_man.topic_pose).wait_for_completed()

    # robot.go_to_pose(meeting_man.time_pose).wait_for_completed()
    # robot.go_to_pose(meeting_man.discussion_pose).wait_for_completed()
    # robot.go_to_pose(meeting_man.group_pose).wait_for_completed()

    robot.go_to_pose(meeting_man.A_pose).wait_for_completed()

    robot.go_to_pose(meeting_man.origin_pose).wait_for_completed()


    # robot.turn_in_place(ang_zc_p, False, 0, ang_speed).wait_for_completed()
    # robot.drive_straight(dist_p_short_for,speed_st_short,False).wait_for_completed()
    # robot.play_anim_trigger(robot.anim_triggers[57]).wait_for_completed()
    # print(robot.pose)
    # robot.go_to_pose(meeting_man.origin_pose).wait_for_completed()

dist_zc = cozmo.util.distance_mm(110) # mm
dist_zc_p = cozmo.util.distance_mm(120) # mm
dist_zc_zc = cozmo.util.distance_mm(150) # mm
dist_p = cozmo.util.distance_mm(200) # mm
dist_p_p = cozmo.util.distance_mm(360) # mm
dist_p_short_for = cozmo.util.distance_mm(150) # mm
dist_p_short_back = cozmo.util.distance_mm(-20) # mm

speed_st = cozmo.util.speed_mmps(300) # mm/s
speed_st_short = cozmo.util.speed_mmps(300) # mm/s

ang_zc_tps = cozmo.util.degrees(-45) # degrees
ang_zc_p = cozmo.util.degrees(45) # degrees
ang_zc_zc = cozmo.util.degrees(135) # degrees
ang_p = cozmo.util.degrees(90) # degrees
ang_p_p = cozmo.util.degrees(130) # degrees
ang_speed = cozmo.util.degrees(180) # degrees