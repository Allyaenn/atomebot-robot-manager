import requests
import json

#### Class Meeting ####
class serverManager: 

    def __init__(self, url):
        print("Server is set up")
        self.URL = url
    
    def requestMessageWithGET(self):
        r = requests.get(url = self.URL +"/msg", params = {}) 
        if r.json() is None : 
            return None
        else :
            return r.json()

    def informServerWithPOST(self, msg):
        print(json.dumps(msg))
        headers = {
            'Content-Type': 'application/json'
            }

        response = requests.request("POST", self.URL, headers=headers, data=json.dumps(msg))
        #requests.post(url = self.URL, json = json.dumps(msg))

# URL = "http://localhost:8888/msg"
  
# do_run = True

# try : 
#     while do_run:
#         time.sleep(10)
#         # sending get request and savinsg the response as response object 
#         r = requests.get(url = URL, params = {}) 
#         if r.json() is None : 
#             print("no message to express");
#         else :
#             print("you've got mail ! : " + json.dumps(r.json(), separators=(',', ':')))
#         #si il y a un message a exprimer -> on l'exprime
#         #sinon on continue
# except KeyboardInterrupt:
#     print("ctrl-c catched - program is interrupted")